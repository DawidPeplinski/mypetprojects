/*
 * main.c
 *
 *  Created on: 3 lis 2015
 *      Author: Dawid
 */

#include <avr/io.h>
#include <util/delay.h>


void C(int a)
{
	int R = 398/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(3.79);
		PORTB &= ~_BV(0); //niski
		_delay_ms(3.79);}
}

void D(int a)
{
	int R = 443/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(3.39);
		PORTB &= ~_BV(0); //niski
		_delay_ms(3.39);}
}

void E(int a)
{
	int R = 498/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(3.01);
		PORTB &= ~_BV(0); //niski
		_delay_ms(3.01);}
}

void F(int a)
{
	int R = 528/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(2.838);
		PORTB &= ~_BV(0); //niski
		_delay_ms(2.838);}
}

void G(int a)
{
	int R = 592/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(2.535);
		PORTB &= ~_BV(0); //niski
		_delay_ms(2.535);}
}

void A(int a)
{
	int R = 664/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(2.26);
		PORTB &= ~_BV(0); //niski
		_delay_ms(2.26);}
}

void B(int a)
{
	int R = 746/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(2.01);
		PORTB &= ~_BV(0); //niski
		_delay_ms(2.01);}
}

void C2(int a)
{
	int R = 793/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.892);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.892);}
}

void D2(int a)
{
	int R = 888/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.69);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.69);}
}

void E2(int a)
{
	int R = 1000/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.5);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.5);}
}

void F2(int a)
{
	int R = 1064/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.41);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.41);}
}

void G2(int a)
{
	int R = 1200/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.25);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.25);}
}

void A2(int a)
{
	int R = 1346/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(1.114);
		PORTB &= ~_BV(0); //niski
		_delay_ms(1.114);}
}

void B2(int a)
{
	int R = 1505/a;
	for(int i=0; i<R; i++)
		{ PORTB |= _BV(0); //wysoki
		_delay_ms(0.997);
		PORTB &= ~_BV(0); //niski
		_delay_ms(0.997);}
}



void p( int a)
{
	int R = 396/a;
		for(int i=0; i<R; i++){
			_delay_ms(7.58);}
}



int main(void)
{
   	DDRB  = 0b00000001;

   	while(1)
   	{
    C(1);
   	D(1);
   	E(1);
   	F(1);
   	G(1);
   	A(1);
   	B(1);
   	C2(1);
   	D2(1);
   	E2(1);
   	F2(1);
   	G2(1);
   	A2(1);
   	B2(1);
   	}

}
