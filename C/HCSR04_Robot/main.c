/*
 * main.c
 *
 *  Created on: 1 gru 2015
 *      Author: Dawid Pepli�ski
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define TRIG_L (1<<PB4)			// definiuj� makra pin�w Triggera i Echo czujnika Lewego i Prawego
#define ECHO_L (1<<PB5)
#define TRIG_P (1<<PB6)
#define ECHO_P (1<<PD4)
#define SILNIK_L_PRZOD (1<<PD0)		// definicja makra steruj�cego mostkiem H, zmiana kierunku obrotu lewego ko�a wykorzystane do zawracania w miejscu
#define SILNIK_L_TYL (1<<PD1)
#define PWM_L OCR2B					// makro definiuj�ce kana�y PWM
#define PWM_P OCR2A

volatile double dist;			// zmienne pomijaj�ce optymalizacj�, wykorzystywane w przerwaniach
volatile unsigned long int time;
volatile uint8_t flag;

double pomiar(uint8_t);
inline void licz(uint8_t);

int main(void)
{
	DDRD |= SILNIK_L_PRZOD | SILNIK_L_TYL | (1<<PD3);
	DDRB |= TRIG_L | TRIG_P | (1<<PB3);		// ustawienie rejestr�w jako wyj�cia i wej�cia
	PORTB &= ~ECHO_L;
	PORTD &= ~ECHO_P;

	PORTD |= SILNIK_L_PRZOD;			//ustawienie obrotu ko�a lewego na mostku H w kierunku: prz�d
	PORTD &= ~SILNIK_L_TYL;

	TCCR0A |= (1<<WGM01);				//tryb timera CTC
	TCCR0B |= (1<<CS00);				// brak preskalera; 1Mhz/1

	TCCR2A |= (1<<COM2A1) | (1<<COM2B1) | (1<<WGM20);	// ustawienie PWM
	TCCR2B |= (1<<CS21) | (1<<CS20);

	sei(); 					// globalne zezwolenie na przerwania

	_delay_ms(1000);

	uint8_t distL = 0;	// zeruj� zmienne pomiarowe
	uint8_t distP = 0;

	while(1)				// p�tla g��wna programu
	{
		distP = pomiar(TRIG_P);			// dokonuj� pomiaru prawym czujnikiem
		distL = pomiar(TRIG_L);			// dokonuj� pomiaru lewym czujnikiem

		PWM_P = 255;					// ustawiam warto�c PWM na maksymaln�, maksymalna pr�dko�c obrotu k�
		PWM_L = 255;
		if(distP<20) PWM_P = 8*distP;	// je�eli odleg�o�c od przeszkody jest mniejsza ni� 20cm, zmniejszaj�c wype�nienie sygna�u steruj�cego pr�dko�ci� ko�a,
		if(distL<20) PWM_L = 8*distL;	// robot zmienia kierunek jazdy omijaj�c przeszkod�
	}
}


double pomiar(uint8_t TRIG)
{
	if(TRIG==TRIG_L){		// je�eli pomiar lewym czujnikem, ustawiam przerwanie lewego Echo
	PCICR |= (1<<PCIE0);
	PCMSK0 |= (1<<PCINT5); }

	else if(TRIG==TRIG_P){			// je�eli pomiar prawym czujnikiem, ustawiam przerwanie prawego Echo
		PCICR |= (1<<PCIE2);
		PCMSK2 |= (1<<PCINT20); }

	PORTB |= TRIG;				// ustawiam Trigger
	_delay_ms(13);
	PORTB &= ~TRIG;				// zeruje Trigger, czujnik wy�le 8 fal d�wi�kowych, ustawi bit ECHO i wyzeruje go kiedy fale wr�c� do czujnika
								// na podstawie d�ugo�ci trwania stanu wysokiego bitu ECHO, okre�lam drog� przebyt� przez d�wi�k, czyli odleg�o�c od przeszkody
	_delay_ms(8);

	return dist;
}


void licz(uint8_t rej)
{
	if(!flag)
		{
			OCR0A = 79;				// przepelnienie licznika wywola przerwanie, mierz� czas trwania sygna�u na ECHO
			TIMSK0 |= (1<<OCIE0A);			// przerwanie przepelnieniem OCR0A
			flag = 1;
		}
		else if(flag)						// po wyzerowaniu stanu na ECHO, przerwanie wraca do funkcji i zatrzymuje timer
		{
			TIMSK0 &= ~(1<<OCIE0A);
			dist = time/59;				// wz�r policzony z pr�dko�ci d�wi�ku, odleg�o�c w cm to czas zmierzony w [us] / ~59
			time = 0;
			PCICR &= ~(1<<rej);
			flag = 0;
		}
}


ISR ( PCINT0_vect )		// przerwanie echo jednego czujnika, funkcja licz() dokonuje pomiaru
{
	licz(PCIE0);
}


ISR ( PCINT2_vect )		// przerwanie echo drugiego czujnika
{
	licz(PCIE2);
}


ISR (TIMER0_COMPA_vect)		// zliczanie czasu trwania sygna�u na Echo
{
	time+=80;
}
