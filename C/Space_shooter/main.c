/*
 * main.c
 *
 *  Created on: 10 gru 2015
 *      Author: Dawid Pepli�ski
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "LCD/lcd44780.h"

#define EEMEM __attribute__((section(".eeprom")))

const char PROGMEM tab1[] = {"FLASH"};
char EEMEM tab2[] = {"EEPROM"};

const uint8_t znak_statek[] PROGMEM = {24,12,30,15,15,30,12,24};
const uint8_t znak_strzal1[] PROGMEM = {0, 0, 0, 31, 0, 0, 0, 0};	// wz�r znaku w pami�ci FLASH
const uint8_t znak_strzal2[] PROGMEM = {0, 0, 0, 0, 31, 0, 0, 0};
const uint8_t znak_przeszk2[] PROGMEM = {0,6,15,31,31,15,14,4};		// reprezentacja bitowa kszta�t�w statku, przeszk�d
const uint8_t znak_przeszk1[] PROGMEM = {12,14,30,31,15,14,4,0};
const uint8_t znak_przeszk3[] PROGMEM = {8,12,31,31,30,14,6,0};
const uint8_t znak_przeszk4[] PROGMEM = {0,4,14,31,30,15,15,6};


volatile uint16_t Timer;
uint8_t speed = 50;			//pr�dko�c pocz�tkowa
uint8_t points = 0;			// punkty pocz�tkowe
uint8_t statek = 0;
uint8_t il = 1;
uint8_t il1 = 0;			// ilo�c przeszk�d na ekranie
uint8_t flag = 0;
void move(uint8_t);		//funkcja przesuwaj�ca statek
void obst_init(void);	// funkcja inicjuj�ca przeszkody

typedef struct		//struktura przeszkody
{
	uint8_t x;
	uint8_t y;
	uint8_t typ;
} PRZESZK;

PRZESZK obst[8];


int main(void)
{
	obst_init();

	DDRC &= ~(1<<PC5);		// ustawiam piny na wej�cia i wyj�cia
	DDRB &= ~(1<<PB1);

	PORTC |= (1<<PC2);
	PORTC |= (1<<PC1);

	lcd_init();
		// za�adowanie znak�w do pami�ci CGRAM
		lcd_defchar_P(0x80, znak_statek);
		lcd_defchar_P(0x81, znak_strzal1);
		lcd_defchar_P(0x82, znak_strzal2);
		lcd_defchar_P(0x83, znak_przeszk1);
		lcd_defchar_P(0x84, znak_przeszk2);
		lcd_defchar_P(0x85, znak_przeszk3);
		lcd_defchar_P(0x86, znak_przeszk4);


	TCCR2 |= (1<<WGM21);			// ustawienie timera od�wie�aj�cego obraz na LCD
	TCCR2 |= (1<<CS22) | (1<<CS21) | (1<<CS20);
	OCR2 = 77;
	TIMSK |= (1<<OCIE2);

	sei();


/*
lcd_locate(0,0);
lcd_str("\x80\x81\x82\x83\x84\x85\x86"); */

	while(1)		// p�tla g��wna programu
	{
		if(!Timer)		// kiedy Timer jest wyzerowany, wykonujemy instrukcje:
		{
			if(!((points+1)%10)){ if(speed>30) speed = speed/2; }	// dla odpowiedniej ilo�ci punkt�w zwi�kszamy pr�dko�c poruszania si� przeszk�d
			lcd_cls();				// wyczyszczenie LCD
			lcd_locate(statek, 0);	// umieszczam kursor na wsp�rz�dnej x = 0; y = statek;
			lcd_str("\x80");		// rysuj� statek

					switch(il1)			// na podstawie ilo�ci przeszk�d, rysuj� je i przesuwam
					{
					default: ;
					case 7: move(7); if(flag) break;
					case 6: move(6); if(flag) break;
					case 5: move(5); if(flag) break;
					case 4: move(4); if(flag) break;
					case 3: move(3); if(flag) break;
					case 2: move(2); if(flag) break;
					case 1: move(1); if(flag) break;
					case 0: move(0); if(flag) break;
					}
					if(flag) { flag = 0; obst_init(); Timer = speed; continue; }
					if(il<30) il++;
					if(il%2) il1++;



			Timer = speed;
		}
	}
}

void move(uint8_t a)
{

	lcd_locate(obst[a].y, obst[a].x);			// ustawiam kursor w miejscu a-tej przeszkody;
	if(obst[a].typ == 0) lcd_str("\x83");			// wybieram typ przeszkody, jej wygl�d i ustawiam na podstawie danej
	else if (obst[a].typ == 1) lcd_str("\x84");
	else if (obst[a].typ == 2) lcd_str("\x85");
	else if (obst[a].typ == 3) lcd_str("\x86");		// poni�ej instrukcja wykrywaj�ca zderzenie statku z przeszkod�, wy�wietlenie wyniku i zako�czenie gry
	if((obst[a].y == statek) && (obst[a].x == 0)) {	lcd_cls(); lcd_locate(0,2); lcd_str("Koniec gry!"); lcd_locate(1,4); lcd_str("Score:"); lcd_int(points); _delay_ms(1500); flag = 1;}
	else if(obst[a].x ==0) { obst[a].x = 15; points++;}

	obst[a].x--;
}

void obst_init(void)
{
	for(int i=0; i<16; i++)
		{								// ustawienie typ�w przeszk�d, inicjalizacja
			obst[i].x = 15;
			obst[i].typ = i%4;
			if(i%2) obst[i].y = 0;
			else obst[i].y = 1;
		}
		speed = 50;		// pocz�tkowa pr�dko�c poruszania si� przeszk�d
		statek = 0;
		il = 1;
		il1 = 0;
		points = 0;
}

ISR(TIMER2_COMP_vect)
{											// przerwanie sprawdzaj�ce wci�ni�cie przycisku, zmieniaj�ce wsp�rz�dn� y statku
	if(!(PINC & (1<<PC5))) statek = 1;
	else if(!(PINB & (1<<PB1)))	statek = 0;
	uint16_t n;
	n = Timer; 	/* 100Hz Timer */
	if(n) Timer = --n;
}
