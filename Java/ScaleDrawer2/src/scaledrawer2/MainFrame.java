package scaledrawer2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle;


public class MainFrame extends JFrame implements ActionListener
{
    private final JPanel myBackground;
    private final JButton bOK;
    private final ButtonGroup bgScale, bgKey, bgType;
    private final JRadioButton rbC, rbCs, rbD, rbDs, rbE, rbF, rbFs, rbG, rbGs, rbA, rbAs, rbB, rbMajor, rbMinor, rbNatural, rbHarmonic, rbPentatonic;
    
       
    public MainFrame()
    {
        setTitle("Scale Drawer");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        //poniżej dodaję elementy okienka
     
      myBackground = new JPanel();
      myBackground.setBackground(Color.BLACK);
     
      bOK = new JButton("OK");
      bOK.addActionListener(this);
        
      bgScale = new ButtonGroup();
      
      rbC = new JRadioButton("C", true);
      rbC.setForeground(Color.white);
      rbC.setBackground(Color.black);
      bgScale.add(rbC);
      
      rbCs = new JRadioButton("C#" ,false);
      rbCs.setForeground(Color.white);
      rbCs.setBackground(Color.black);
      bgScale.add(rbCs);
      
      rbD = new JRadioButton("D" ,false);
      rbD.setForeground(Color.white);
      rbD.setBackground(Color.black);
      bgScale.add(rbD);
      
      rbDs = new JRadioButton("D#" ,false);
      rbDs.setForeground(Color.white);
      rbDs.setBackground(Color.black);
      bgScale.add(rbDs);
      
      rbE = new JRadioButton("E" ,false);
      rbE.setForeground(Color.white);
      rbE.setBackground(Color.black);
      bgScale.add(rbE);
      
      rbF = new JRadioButton("F" ,false);
      rbF.setForeground(Color.white);
      rbF.setBackground(Color.black);
      bgScale.add(rbF);
      
      rbFs = new JRadioButton("F#" ,false);
      rbFs.setForeground(Color.white);
      rbFs.setBackground(Color.black);
      bgScale.add(rbFs);
      
      rbG = new JRadioButton("G" ,false);
      rbG.setForeground(Color.white);
      rbG.setBackground(Color.black);
      bgScale.add(rbG);
      
      rbGs = new JRadioButton("G#" ,false);
      rbGs.setForeground(Color.white);
      rbGs.setBackground(Color.black);
      bgScale.add(rbGs);
      
      rbA = new JRadioButton("A" ,false);
      rbA.setForeground(Color.white);
      rbA.setBackground(Color.black);
      bgScale.add(rbA);
      
      rbAs = new JRadioButton("A#" ,false);
      rbAs.setForeground(Color.white);
      rbAs.setBackground(Color.black);
      bgScale.add(rbAs);
      
      rbB = new JRadioButton("B" ,false);
      rbB.setForeground(Color.white);
      rbB.setBackground(Color.black);
      bgScale.add(rbB);
      
      
      bgKey = new ButtonGroup();
      
      rbMajor = new JRadioButton("Major", true);
      rbMajor.setForeground(Color.white);
      rbMajor.setBackground(Color.black);
      bgKey.add(rbMajor);
      
      rbMinor = new JRadioButton("Minor", false);
      rbMinor.setForeground(Color.white);
      rbMinor.setBackground(Color.black);
      bgKey.add(rbMinor);
      
      
      bgType = new ButtonGroup();
      
      rbNatural = new JRadioButton("Natural", true);
      rbNatural.setForeground(Color.white);
      rbNatural.setBackground(Color.black);
      bgType.add(rbNatural);
      
      rbHarmonic = new JRadioButton("Harmonic", false);
      rbHarmonic.setForeground(Color.white);
      rbHarmonic.setBackground(Color.black);
      bgType.add(rbHarmonic);
      
      rbPentatonic = new JRadioButton("Pentatonic", false);
      rbPentatonic.setForeground(Color.white);
      rbPentatonic.setBackground(Color.black);
      bgType.add(rbPentatonic);
      
      
      //definujemy wygląd okna, grupę panelu
      GroupLayout myBackgroundLayout = new GroupLayout(myBackground);
      myBackground.setLayout(myBackgroundLayout);
      
   myBackgroundLayout.setHorizontalGroup(
            myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(myBackgroundLayout.createSequentialGroup()
                .addGroup(myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(myBackgroundLayout.createSequentialGroup()
                        .addGap(400, 400, 400)
                        .addComponent(bOK))
                    .addGroup(myBackgroundLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(rbC)
                            .addComponent(rbCs)
                            .addComponent(rbD)
                            .addComponent(rbDs)
                            .addComponent(rbE)
                            .addComponent(rbF)
                            .addComponent(rbFs)
                            .addComponent(rbG)
                            .addComponent(rbGs)
                            .addComponent(rbA)
                            .addComponent(rbAs)
                            .addComponent(rbB)))
                        .addGroup(myBackgroundLayout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addGroup(myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(rbMajor)
                            .addComponent(rbMinor)))
                        .addGroup(myBackgroundLayout.createSequentialGroup()
                        .addGap(300, 300, 300)
                        .addGroup(myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(rbNatural)
                            .addComponent(rbHarmonic)
                            .addComponent(rbPentatonic)))
                )
                .addContainerGap(50, Short.MAX_VALUE))
        );
   
        myBackgroundLayout.setVerticalGroup(
            myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(myBackgroundLayout.createSequentialGroup()
                .addGroup(myBackgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                  .addGroup(myBackgroundLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(rbC)
                .addComponent(rbCs)
                .addComponent(rbD)
                .addComponent(rbDs)
                .addComponent(rbE)
                .addComponent(rbF)
                .addComponent(rbFs)
                .addComponent(rbG)
                .addComponent(rbGs)
                .addComponent(rbA)
                .addComponent(rbAs)
                .addComponent(rbB)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(bOK)
                )
                .addGroup(myBackgroundLayout.createSequentialGroup()
                   .addGap(100, 100, 100)
                .addComponent(rbMajor)
                   .addGap(100, 100, 100)
                .addComponent(rbMinor))
                .addGroup(myBackgroundLayout.createSequentialGroup()
                   .addGap(100, 100, 100)
                .addComponent(rbNatural)
                   .addGap(40, 40, 40)
                .addComponent(rbHarmonic)
                    .addGap(40, 40, 40)
                .addComponent(rbPentatonic))
                )
           .addContainerGap(20, Short.MAX_VALUE) )
        );
      
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(myBackground, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(myBackground, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        
    }
    
    
    public static void MainFrameInit()  
    {
        MainFrame okno = new MainFrame();
    }

       
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        Object mySrc = e.getSource();
        if(mySrc==bOK)
        {
            boolean ifMajor = rbMajor.isSelected();
            boolean ifNatural = rbNatural.isSelected();
            boolean ifPentatonic = rbPentatonic.isSelected();
            if(rbC.isSelected()) ScaleCompute.Compute(0, ifMajor, ifNatural, ifPentatonic);
            else if(rbCs.isSelected()) ScaleCompute.Compute(1, ifMajor, ifNatural, ifPentatonic);
            else if(rbD.isSelected()) ScaleCompute.Compute(2, ifMajor, ifNatural, ifPentatonic);
            else if(rbDs.isSelected()) ScaleCompute.Compute(3, ifMajor, ifNatural, ifPentatonic);
            else if(rbE.isSelected()) ScaleCompute.Compute(4, ifMajor, ifNatural, ifPentatonic);
            else if(rbF.isSelected()) ScaleCompute.Compute(5, ifMajor, ifNatural, ifPentatonic);
            else if(rbFs.isSelected())  ScaleCompute.Compute(6, ifMajor, ifNatural, ifPentatonic);
            else if(rbG.isSelected()) ScaleCompute.Compute(7, ifMajor, ifNatural, ifPentatonic);
            else if(rbGs.isSelected()) ScaleCompute.Compute(8, ifMajor, ifNatural, ifPentatonic);
            else if(rbA.isSelected()) ScaleCompute.Compute(9, ifMajor, ifNatural, ifPentatonic);
            else if(rbAs.isSelected()) ScaleCompute.Compute(10, ifMajor, ifNatural, ifPentatonic);
            else if(rbB.isSelected()) ScaleCompute.Compute(11, ifMajor, ifNatural, ifPentatonic);
        }
    }
    
    
}
