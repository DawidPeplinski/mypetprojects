
package scaledrawer2;

import java.awt.Color;
import javax.swing.JLabel;


public class ScaleCompute extends ScaleClass2
{
    private static int intIncrease(int f)
    {
        f++;
        switch (f)
        {
            case 12: f = 0; break;
            case 13: f = 1; break;
            default: break;
        }       
        return f;
    }
    
    private static void checkSounds(int i, int j, int c)
    {
        for(int z = 0; z < mySounds.length; z++)
        {
            if(mySounds[z].equals(allSounds[c+j])) //porównuję dźwięki z wybranej skali z wszystkimi dźwiękami gryfu i wybieram tylko te równoważne
            {
                fretboardSounds[i][j] = new JLabel(allSounds[c+j]); 
                fretboardSounds[i][j].setOpaque(true);
                fretboardSounds[i][j].setFont(new java.awt.Font("Serif", java.awt.Font.BOLD, 19));

                if(allSounds[c+j].equals(mySounds[0]))
                {
                    fretboardSounds[i][j].setBackground(Color.red); 
                    fretboardSounds[i][j].setForeground(Color.white);   
                }
                else
                {
                    fretboardSounds[i][j].setBackground(new Color(255, 255, 255)); 
                    fretboardSounds[i][j].setForeground(Color.black);
                }
                break;   // znalazłem dźwięk pasujący, opuszczam pętlę
            }
            else 
            {
                fretboardSounds[i][j] = new JLabel(" "); 
            }
        }

    }
    
    public static void Compute(int firstSound, boolean ifMajor, boolean ifNatural, boolean ifPentatonic)
    {
        title = allSounds[firstSound];
        if(ifMajor) title += " Major";
        else title += " Minor";
        if(ifNatural) title += " Natural";
        else if(!ifPentatonic) title += " Harmonic";
        else if(ifPentatonic) title += " Pentatonic";
        
        if(ifMajor)
        {
                int num = firstSound;
                mySounds[0] = allSounds[num];
                myChords[0] = allSounds[num] + "-major";
                num = intIncrease(num);
                mySounds[1] = " ";
                myChords[1] = " ";
                num = intIncrease(num);
                mySounds[2] = allSounds[num];
                if(ifNatural) myChords[2] = allSounds[num] + "-minor";
               else if(!ifNatural) myChords[2] = allSounds[num] + "-dim";
                num = intIncrease(num);
                mySounds[3] = " ";
                myChords[3] = " ";
                num = intIncrease(num);
                mySounds[4] = allSounds[num];
                myChords[4] = allSounds[num] + "-minor";
                num = intIncrease(num);
               if(!ifPentatonic) mySounds[5] = allSounds[num];              // w pentatonicznej usuwam stopień IV oraz VII
               else if(ifPentatonic) mySounds[5] = " ";
                if(ifNatural) myChords[5] = allSounds[num] + "-major";
               else if(!ifNatural) myChords[5] = allSounds[num] + "-minor";
                num = intIncrease(num);
                mySounds[6] = " ";
                myChords[6] = " ";
                num = intIncrease(num);
                mySounds[7] = allSounds[num];
                myChords[7] = allSounds[num] + "-major";
                num = intIncrease(num);
               if(ifNatural || ifPentatonic)
               {
                mySounds[8] = " ";
                myChords[8] = " ";
                num = intIncrease(num);
                mySounds[9] = allSounds[num];
                myChords[9] = allSounds[num] + "-minor";
               }
               else if(!ifNatural && !ifPentatonic)
               {
                mySounds[8] = allSounds[num];
                myChords[8] = allSounds[num] + "-aug";
                num = intIncrease(num);
                mySounds[9] = " ";
                myChords[9] = " ";
               }
                num = intIncrease(num);
                mySounds[10] = " ";
                myChords[10] = " ";
                num = intIncrease(num);
                if(!ifPentatonic) mySounds[11] = allSounds[num];              // w pentatonicznej usuwam stopień IV oraz VII
               else if(ifPentatonic) mySounds[11] = " ";
                myChords[11] = allSounds[num] + "-dim";
        }
        else if(!ifMajor)
        {
                int num = firstSound;
                mySounds[0] = allSounds[num];
                myChords[0] = allSounds[num] + "-minor";
                num = intIncrease(num);
                mySounds[1] = " ";
                myChords[1] = " ";
                num = intIncrease(num);
                mySounds[2] = allSounds[num];
                if(ifPentatonic) mySounds[2] = " ";
                myChords[2] = allSounds[num] + "-dim"; // w pentatonicznej usuwam stopień II oraz VI
                num = intIncrease(num);
                mySounds[3] = allSounds[num];
               if(ifNatural) myChords[3] = allSounds[num] + "-major";
               else if(!ifNatural) myChords[3] = allSounds[num] + "-aug";
                num = intIncrease(num);
                mySounds[4] = " ";
                myChords[4] = " ";
                num = intIncrease(num);
                mySounds[5] = allSounds[num];
                myChords[5] = allSounds[num] + "-minor";
                num = intIncrease(num);
                mySounds[6] = " ";
                myChords[6] = " ";
                num = intIncrease(num);
                mySounds[7] = allSounds[num];
                myChords[7] = allSounds[num] + "-major";
                num = intIncrease(num);
                mySounds[8] = allSounds[num];
                if(ifPentatonic) mySounds[8] = " ";
                myChords[8] = allSounds[num] + "-major";
                num = intIncrease(num);
                mySounds[9] = " ";
                myChords[9] = " ";
                num = intIncrease(num);
               if(ifNatural || ifPentatonic)
               {
                mySounds[10] = allSounds[num];
                myChords[10] = allSounds[num] + "-major";
                mySounds[11] = " "; 
                myChords[11] = " ";
               }
               else if(!ifNatural && !ifPentatonic)
               {
                mySounds[10] = " ";
                myChords[10] = " ";
                num = intIncrease(num);
                mySounds[11] = allSounds[num]; 
                myChords[11] = allSounds[num] + "-dim";
               }
        }
        
       for(int i = 0; i<6; i++)
       {
           int c = 0;           //stała odnosząca się do dźwięków zerowego progu gitary
           switch(i)
           {
               case 0: c = 4; break;
               case 1: c = 11; break;
               case 2: c = 7; break;
               case 3: c = 2; break;
               case 4: c = 9; break;
               case 5: c = 4; break;
               default: ;
           }
           for(int j = 0; j<13; j++)
           {
               switch(i)
               {
                   case 0: if((c + j) > 11) c = -8; checkSounds(i, j, c); break;
                   case 1: if((c + j) > 11) c = -1; checkSounds(i, j, c); break;
                   case 2: if((c + j) > 11) c = -5; checkSounds(i, j, c); break;
                   case 3: if((c + j) > 11) c = -10; checkSounds(i, j, c); break;
                   case 4: if((c + j) > 11) c = -3; checkSounds(i, j, c); break;
                   case 5: if((c + j) > 11) c = -8; checkSounds(i, j, c); break;
                   default: ;
               }
           }
       }
        
        ScaleClass2Init(ifPentatonic);
    }

    public ScaleCompute(boolean ifPentatonic) 
    {
        super(ifPentatonic);
    }
    
}
