
package scaledrawer2;

import java.awt.*;
import javax.swing.*;

public class ScaleClass2 extends JFrame  {

    private final JPanel scaleBackground;
    private final JLabel fretboard;
    private JLayeredPane l = null;
    
    protected static String title;
    protected static JLabel[][] fretboardSounds = new JLabel[6][13];
    protected static String[] allSounds = {" C", "C#", " D", "D#", " E", " F", "F#", " G", "G#", " A", "A#", " B" };       
    protected static String[] mySounds = new String[12];
    protected static String[] myChords = new String[12];
    
 
  public ScaleClass2(boolean ifPentatonic) 
  {  
    l = getLayeredPane();
    getContentPane().setBackground(Color.black); 
   
    setTitle(title);
    setDefaultCloseOperation(HIDE_ON_CLOSE);
    setVisible(true);
    setResizable(true);
        
     scaleBackground = new JPanel();      
     scaleBackground.setBackground(Color.black);
     scaleBackground.setBounds(0, 0, 400, 300);
     l.add(scaleBackground, new Integer(1));
     
     ImageIcon SortByIcon = new ImageIcon(getClass().getClassLoader().getResource("gryf3.jpg"));  // obrazek do projektu
     fretboard = new JLabel(SortByIcon);
     fretboard.setBounds(0, 0, 1200, 231);
     l.add(fretboard, new Integer(2));
     
     for(int i = 0; i<6; i++)
       {
           for(int j = 0; j<13; j++)
           {
               switch (j)
               {
                   case 0: fretboardSounds[i][j].setBounds(0, 5 + i*40-i, 25, 25);  break;
                   case 1: fretboardSounds[i][j].setBounds(j*75, 5 + i*40-i, 25, 25);  break;
                   case 2: fretboardSounds[i][j].setBounds(j*98, 5 + i*40-i, 25, 25);  break;
                   case 3: fretboardSounds[i][j].setBounds(j*106, 5 + i*40-i, 25, 25);  break;
                   case 4: fretboardSounds[i][j].setBounds(j*107, 5 + i*40-i, 25, 25);  break;
                   case 5: fretboardSounds[i][j].setBounds(j*108, 5 + i*40-i, 25, 25);  break;
                   case 6: fretboardSounds[i][j].setBounds(j*107, 5 + i*40-i, 25, 25);  break;
                   case 7: fretboardSounds[i][j].setBounds(j*105, 5 + i*40-i, 25, 25);  break;
                   case 8: fretboardSounds[i][j].setBounds(j*103, 5 + i*40-i, 25, 25);  break;
                   case 9: fretboardSounds[i][j].setBounds(j*102, 5 + i*40-i, 25, 25);  break;
                   case 10: fretboardSounds[i][j].setBounds(j*100-3, 5 + i*40-i, 25, 25);  break;
                   case 11: fretboardSounds[i][j].setBounds(j*97, 5 + i*40-i, 25, 25);  break;
                   case 12: fretboardSounds[i][j].setBounds(j*95+5, 5 + i*40-i, 25, 25);  break;
                   default: break;
               }
               l.add(fretboardSounds[i][j], new Integer(3));
           }
       }

    if(!ifPentatonic)
    {
    JLabel b = new JLabel("Chords: ");
    b.setBounds(45, 260, 75, 40);
    b.setForeground(Color.white);
    b.setFont(new java.awt.Font("Serif", java.awt.Font.BOLD, 19));
    l.add(b, new Integer(3));
    
     int x = 45;
     for (int i = 0; i < 12; i++) {
      if(myChords[i].equals(" ")) continue;
      b = new JLabel(myChords[i]);
      b.setBounds(80+x, 260, 90, 40);
      b.setForeground(Color.white);
      b.setFont(new java.awt.Font("Serif", java.awt.Font.BOLD, 19));
      l.add(b, new Integer(3));
      x+=100;
    }
    }
    
    setSize(1215, 351);
    setVisible(true);
  }

  
  public static void ScaleClass2Init(boolean ifPentatonic) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new ScaleClass2(ifPentatonic);
      }
    });
  }

}
