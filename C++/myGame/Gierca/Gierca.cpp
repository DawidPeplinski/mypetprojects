// Gierca.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Gierca.h"
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <string>

#define MAX_LOADSTRING 100
#define TMR_1 1

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

INT v;
INT i;
INT L;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

RECT drawing_area = { 0, 0, (int)GetSystemMetrics(SM_CXSCREEN), 650 };


struct kwad
{
	int x;
	int y;
	bool cz;
};

kwad tabkwad[35];
struct statek
{
	int x;
	int y;
	int hp;
	int pkt;
};

statek stat;

void statek(HDC hdc)
{
	Graphics graphics(hdc);
	Pen pen(Color(255, 255, 0, 0), 5);
	
	TCHAR buf[256];
	
	
	swprintf_s(buf, 256, L"Points: %i", stat.pkt);
	TextOut(hdc, 50, 5, buf, _tcslen(buf));
	swprintf_s(buf, 256, L"Speed: %i", L);
	TextOut(hdc, 150, 5, buf, _tcslen(buf));




	graphics.DrawLine(&pen, 50 + stat.x, 275 + stat.y, 50 + stat.x, 325 + stat.y);
	graphics.DrawLine(&pen, 50 + stat.x, 275 + stat.y, 100 + stat.x, 300 + stat.y);
	graphics.DrawLine(&pen, 50 + stat.x, 325 + stat.y, 100 + stat.x, 300 + stat.y);

}

void przeszk(HDC hdc, int a)
{
	Graphics graphics(hdc);
	Pen pen(Color(255, 0, 0, 0), 5);

	if (tabkwad[a].x == 0)
	{
		int h = rand() % 515; tabkwad[a].y = h;
	}	
	
	if (tabkwad[a].x >= 0)
	graphics.DrawRectangle(&pen, 1420 - L*2*tabkwad[a].x , 30 + tabkwad[a].y, 50, 50);
	
	if (stat.pkt < 10) 	tabkwad[a].x++;
	else if (stat.pkt >= 10) tabkwad[a].x += 2;

	if (L*2*tabkwad[a].x > 1470) {
		tabkwad[a].x = -50; tabkwad[a].y = 0; stat.pkt += L; }

}


void MyOnPaint(HDC hdc, HWND hWnd)
{
	
	
	float point_radius = 15;
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	HDC hdcMem = CreateCompatibleDC(hdc);

	const int nMemDC = SaveDC(hdcMem);

	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
	SelectObject(hdcMem, hBitmap);

	Graphics graphics(hdcMem);
	SolidBrush back(Color(255, 255, 255));
	SolidBrush points(Color(255, 0, 0));

	graphics.FillRectangle(&back, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

	
	statek(hdcMem);
	Pen pen(Color(255, 0, 0, 255));

	graphics.DrawRectangle(&pen, 0, 0, 1420, 25);
	graphics.DrawRectangle(&pen, 0, 600, 1420, 25);
	graphics.DrawRectangle(&pen, 0, 0, 25, 625);
	graphics.DrawRectangle(&pen, 1395, 0, 25, 625);

	v++;
	switch (i)
	{
	default:;

	case 8:  {int a = 8; przeszk(hdcMem, a); }

	case 7:  {int a = 7; przeszk(hdcMem, a);  }

	case 6:  {int a = 6; przeszk(hdcMem, a);  }

	case 5:  {int a = 5; przeszk(hdcMem, a);  }

	case 4:  {int a = 4; przeszk(hdcMem, a);  }

	case 3:  {int a = 3; przeszk(hdcMem, a); }

	case 2:  {int a = 2; przeszk(hdcMem, a); }

	case 1:  {int a = 1; przeszk(hdcMem, a); }


	}
	if (v == i * 50 && i < 8) { i++; }

	RECT rcClip;
	GetClipBox(hdc, &rcClip);

	BitBlt(hdc, rcClip.left, rcClip.top, rcClip.right - rcClip.left, rcClip.bottom - rcClip.top, hdcMem, rcClip.left, rcClip.top, SRCCOPY);

	RestoreDC(hdcMem, nMemDC);
	DeleteObject(hBitmap);
	DeleteObject(hdcMem);


}


int OnCreate(HWND window)
{
	SetTimer(window, TMR_1, 25, 0);
	return 0;
}


int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;
	v = 0;
	i = 1;
	L = 2;
	srand(time(NULL));
	for (int i = 0; i < 25; i++)
	{
		tabkwad[i].x = 0;
		tabkwad[i].y = 0;
	}
	stat.x = 0;
	stat.y = 0;
	stat.pkt = 0;

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_GIERCA, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GIERCA));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	GdiplusShutdown(gdiplusToken);
	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GIERCA));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_GIERCA);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
  
   OnCreate(hWnd);
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT: {
					   hdc = BeginPaint(hWnd, &ps);
					   MyOnPaint(hdc, hWnd);
					   // TODO: Add any drawing code here...
					   EndPaint(hWnd, &ps);
					   break;
	}
	case WM_ERASEBKGND:
		return 1;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_TIMER:
		switch (wParam)
		{
		case TMR_1:
			//force window to repaint
			InvalidateRect(hWnd, &drawing_area, TRUE);
			UpdateWindow(hWnd);
			break;
		}

	case WM_KEYDOWN:
	{
					   switch (wParam) {
					   case VK_RIGHT:
					   {

										if (stat.x < 1290){
											InvalidateRect(hWnd, NULL, FALSE);
											hdc = BeginPaint(hWnd, &ps);
											stat.x+=20;
											EndPaint(hWnd, &ps);
										}
										break;
					   }
					   case VK_LEFT:
					   {
									   if (stat.x > -20){
										   InvalidateRect(hWnd, NULL, FALSE);
										   hdc = BeginPaint(hWnd, &ps);
										   stat.x-=20;
										   EndPaint(hWnd, &ps);
									   }
									   break;
					   }
					   case VK_DOWN:
					   {
									   if (stat.y < 270){
										   InvalidateRect(hWnd, NULL, FALSE);
										   hdc = BeginPaint(hWnd, &ps);
										   stat.y += 20;
										   EndPaint(hWnd, &ps);
									   }
									   break;
					   }
					   case VK_UP:
					   {
									 if (stat.y>-245){
										 InvalidateRect(hWnd, NULL, FALSE);
										 hdc = BeginPaint(hWnd, &ps);
										 stat.y -= 20;
										 EndPaint(hWnd, &ps);
									 }
									 break;
					   }
					   }}
						   //case VK_
					  
	

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
