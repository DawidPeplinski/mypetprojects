// Projekt 4.cpp : program wczytuje warto�ci odczyt�w czujnik�w robota z pliku, rysuje na ich podstaie wykresy i pozwala je analizowa�
//

#include "stdafx.h"
#include "Projekt 4.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <cmath>

#define MAX_LOADSTRING 100
#define TMR_1 1

using namespace std;
// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

RECT drawing_area = { 0, 0, (int)GetSystemMetrics(SM_CXSCREEN), 650 };

vector<float> all;
vector<int> PosX;
vector<int> v;
vector<int> s;

HWND g_hPrzycisk1, g_hPrzycisk2, g_hPrzycisk3, g_hPrzycisk4, g_hPrzycisk5, g_hPrzycisk6, g_hPrzycisk7, g_hPrzycisk8, g_hPrzycisk9, g_hPrzycisk10;
INT a, b, d;        // a przesuniecie, b amplituda, d pomiarka czasowa
DOUBLE G;
BOOL usun, przysp, V, droga;

void czytaj()
{
	fstream plik;
	plik.open("outputRobotForwardB03.log", ios::in);
	if (!plik) exit(1);

	int a = plik.good();
	while (!plik.eof())
	{
		float b;
		plik >> b;
		all.push_back(b);
	}
	int b = all.size();
	for (int i = 3; i < b; i += 12){
		PosX.push_back((int)(all[i] * 1000));
	}


	plik.close();
}


void MyOnPaint(HDC hdc, HWND hWnd)
{
	float point_radius = 15;
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	HDC hdcMem = CreateCompatibleDC(hdc);

	const int nMemDC = SaveDC(hdcMem);

	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
	SelectObject(hdcMem, hBitmap);

	Graphics graphics(hdcMem);
	SolidBrush back(Color(255, 255, 255));
	SolidBrush points(Color(255, 0, 0));

	graphics.FillRectangle(&back, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

	///////////////////////////////////////

	Pen pen(Color(255, 100, 100, 100));
	Pen pen1(Color(255, 0, 0, 0));
	Pen pen2(Color(64, 0, 0, 0));
	Pen pen3(Color(255, 255, 0, 0));
	Pen pen4(Color(255, 0, 0, 255));

	graphics.DrawLine(&pen1, 0, 400, 2000, 400);

	SolidBrush  brush(Color(255, 100, 100, 100));
	SolidBrush  brush1(Color(255, 255, 0, 0));
	SolidBrush  brush2(Color(255, 0, 0, 255));

	FontFamily  fontFamily(L"Times New Roman");
	Font        font(&fontFamily, 24, FontStyleRegular, UnitPixel);
	PointF      pointF(250.0f, 450.0f);
	PointF      pointF1(400.0f, 450.0f);
	PointF      pointF2(550.0f, 450.0f);


	graphics.DrawString(L"a [m/s^2]", -1, &font, pointF, &brush);
	graphics.DrawString(L"v [m/s]", -1, &font, pointF1, &brush1);
	graphics.DrawString(L"s [m]", -1, &font, pointF2, &brush2);



	for (int i = 0; i < 2100; i += 25) { 
	graphics.DrawLine(&pen1, i, 395, i, 405);
	TCHAR buf[256];
	swprintf_s(buf, 256, L"%i", i/25);
	TextOut(hdcMem, a+d*(i-4), 406, buf, _tcslen(buf));
}
	if (b < 3) {
		for (int i = 0; i < 120; i += 20)
		{
			TCHAR buf[256];
			swprintf_s(buf, 256, L"%i", i / 10);
			TextOut(hdcMem, 0, (192 - (b*i)), buf, _tcslen(buf));
			swprintf_s(buf, 256, L"-%i", i / 10);
			TextOut(hdcMem, 0, (192 + (b*i)), buf, _tcslen(buf));
			graphics.DrawLine(&pen2, 20, (200 - (b*i)), 2000, (200 - (b*i)));
			graphics.DrawLine(&pen2, 20, (200 + (b*i)), 2000, (200 + (b*i)));
		}
	}
	else if (b<6){
		for (int i = 0; i < 50; i += 10)
		{
			TCHAR buf[256];
			swprintf_s(buf, 256, L"%i", i / 10);
			TextOut(hdcMem, 0, (192 - (b*i)), buf, _tcslen(buf));
			swprintf_s(buf, 256, L"-%i", i / 10);
			TextOut(hdcMem, 0, (192 + (b*i)), buf, _tcslen(buf));
			graphics.DrawLine(&pen2, 20, (200 - (b*i)), 2000, (200 - (b*i)));
			graphics.DrawLine(&pen2, 20, (200 + (b*i)), 2000, (200 + (b*i)));
		}
	}
	else if (b<10){
		for (int i = 0; i <=20; i += 5)
		{
			TCHAR buf[256];
			if (i !=15 && i !=5){
				swprintf_s(buf, 256, L"%i", i / 10);
				TextOut(hdcMem, 0, (192 - (b*i)), buf, _tcslen(buf));
				swprintf_s(buf, 256, L"-%i", i / 10);
				TextOut(hdcMem, 0, (192 + (b*i)), buf, _tcslen(buf));
			}
			else {
				swprintf_s(buf, 256, L"%i,5", i / 10);
				TextOut(hdcMem, 0, (192 - (b*i)), buf, _tcslen(buf));
				swprintf_s(buf, 256, L"-%i,5", i / 10);
				TextOut(hdcMem, 0, (192 + (b*i)), buf, _tcslen(buf));
			}
			graphics.DrawLine(&pen2, 20, (200 - (b*i)), 2000, (200 - (b*i)));
			graphics.DrawLine(&pen2, 20, (200 + (b*i)), 2000, (200 + (b*i)));
		}
	}



	int c = PosX.size();
	if (przysp)
	for (int i = 1; i < c; i++){
		graphics.DrawLine(&pen, a+d*(i-1), (200 - (b*PosX[(i - 1)])/10), a+d*i, (200 - (b*PosX[i])/10));
	}
	
	if(usun && V)
	{
		for (int i = 1; i < c; i++){
			graphics.DrawLine(&pen3, a + d*(i - 1), (200 - (b*v[(i - 1)])/10), a + d*i, (200 - (b*v[i])/10));
		}
	}

	if (usun && droga)
	{
		int c = s.size();
		for (int i = 1; i < c; i++){
			graphics.DrawLine(&pen4, a + d*(i - 1), (200 - (b*s[(i - 1)]) /10), a + d*i, (200 - (b*s[i]) / 10));
			
		}
	}

	/////////////////////////////////////

	RECT rcClip;
	GetClipBox(hdc, &rcClip);

	BitBlt(hdc, rcClip.left, rcClip.top, rcClip.right - rcClip.left, rcClip.bottom - rcClip.top, hdcMem, rcClip.left, rcClip.top, SRCCOPY);

	RestoreDC(hdcMem, nMemDC);
	DeleteObject(hBitmap);
	DeleteObject(hdcMem);
}


int OnCreate(HWND window)
{
	SetTimer(window, TMR_1, 50, 0);
	return 0;
}




int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;
	czytaj();
	a = 0;
	b = 1;
	d = 1;
	G = 9.81;
	usun = FALSE;
	przysp = FALSE;
	V = FALSE;
	droga = FALSE;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROJEKT4, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJEKT4));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	GdiplusShutdown(gdiplusToken);
	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJEKT4));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PROJEKT4);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

     g_hPrzycisk1 = CreateWindowEx(0, L"BUTTON", L"+", WS_CHILD | WS_VISIBLE, 0, 420, 50, 30, hWnd, NULL, hInstance, NULL);                  
     g_hPrzycisk2 = CreateWindowEx(0, L"BUTTON", L"-", WS_CHILD | WS_VISIBLE, 0, 450, 50, 30, hWnd, NULL, hInstance, NULL);		
	 g_hPrzycisk3 = CreateWindowEx(0, L"BUTTON", L"<-", WS_CHILD | WS_VISIBLE, 50, 480, 50, 30, hWnd, NULL, hInstance, NULL);
	 g_hPrzycisk4 = CreateWindowEx(0, L"BUTTON", L"->", WS_CHILD | WS_VISIBLE, 100, 480, 50, 30, hWnd, NULL, hInstance, NULL);
	 g_hPrzycisk5 = CreateWindowEx(0, L"BUTTON", L"-", WS_CHILD | WS_VISIBLE, 0, 480, 25, 30, hWnd, NULL, hInstance, NULL);
	 g_hPrzycisk6 = CreateWindowEx(0, L"BUTTON", L"+", WS_CHILD | WS_VISIBLE, 25, 480, 25, 30, hWnd, NULL, hInstance, NULL);
	 g_hPrzycisk7 = CreateWindowEx(0, L"BUTTON", L"Usun stala skladowa [Wylicz v i s]", WS_CHILD | WS_VISIBLE, 180, 480, 240, 30, hWnd, NULL, hInstance, NULL);
	 g_hPrzycisk8 = CreateWindowEx(WS_EX_TRANSPARENT, L"button", L"Przyspieszenie", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 450, 480, 125, 20, hWnd, 0, hInst, NULL);
	 g_hPrzycisk9 = CreateWindowEx(WS_EX_TRANSPARENT, L"button", L"Predkosc", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 450, 500, 125, 20, hWnd, 0, hInst, NULL);
	 g_hPrzycisk10 = CreateWindowEx(WS_EX_TRANSPARENT, L"button", L"Droga", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 450, 520, 125, 20, hWnd, 0, hInst, NULL);


   OnCreate(hWnd);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);

		if ((HWND)lParam == g_hPrzycisk1) { if (b < 9) b = b + 1; }           
		if ((HWND)lParam == g_hPrzycisk2) { if (b>1) b = b - 1; }
		if ((HWND)lParam == g_hPrzycisk3) {  a+=d*25; }         
		if ((HWND)lParam == g_hPrzycisk4) {  a-=d*25; }
		if ((HWND)lParam == g_hPrzycisk5) if (d>1){ d--; }
		if ((HWND)lParam == g_hPrzycisk6) { d++; }
		if ((HWND)lParam == g_hPrzycisk7) {
			if (!usun){
				for (int z = 0; z < 10; z++) {
					long double S = 0;
					int b = PosX.size();
					for (int i = 0; i < b; i++)
					{
						S = S + PosX[i];
					}
					S = S / b;
					for (int i = 0; i < b; i++)
					{
						PosX[i] = PosX[i] - S;
					}
				}

				int u = PosX.size();
				long double predk = 0;
				for (int i = 0; i < u; i++)
				{
					if (PosX[i]>10 || PosX[i]<-10){
						predk = predk + 0.04*PosX[i];
						v.push_back((int)(predk));
					}
					else	v.push_back((int)(predk));


				}

				int y = v.size();
				long double drog = 0;
				for (int i = 0; i < y; i++)
				{
						drog = drog + 0.04*abs(v[i]);
						s.push_back((int)(drog));
				}

				usun = TRUE;
			}
		}
		if ((HWND)lParam == g_hPrzycisk8) {
			if (przysp) przysp = FALSE;
			else przysp = TRUE;
		}

		if ((HWND)lParam == g_hPrzycisk9) {
			if (V) V = FALSE;
			else V = TRUE;
		}

		if ((HWND)lParam == g_hPrzycisk10) {
			if (droga) droga = FALSE;
			else droga = TRUE;
		}
		
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:{
			  hdc = BeginPaint(hWnd, &ps);
			  MyOnPaint(hdc, hWnd);
			  EndPaint(hWnd, &ps);
			  break;
	}
	case WM_ERASEBKGND:
		return 1;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_TIMER:
		switch (wParam)
		{
		case TMR_1:
			//force window to repaint
			InvalidateRect(hWnd, &drawing_area, TRUE);
			UpdateWindow(hWnd);
			break;
		}

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
